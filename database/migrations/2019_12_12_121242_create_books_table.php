<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('genre', 50);
            $table->string('authorName', 100);
            $table->date('date_released');
            $table->unsignedBigInteger('quantity', 50);
			$table->string('display', 100);
			$table->string('description');
            $table->unsignedBigInteger('views');
            $table->unsignedBigInteger('is_archived');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
