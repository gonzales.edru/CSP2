<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\books;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\borrow_requests;
class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = books::all();
        $genres = books::distinct()->get('genre');	
        return view('home', ['books' => $books, 'genres'=>$genres ]); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new books;
        $view = 0;
        $book->name = $request->name;
        $book->authorName = $request->author;
        $book->date_released = $request->date_released;
        $book->genre = $request->genre;
        $path = $request->image->store('images', 'public');
        $book->display = $path;
        $book->description = $request->description;
        $book->views = $view;
        $book->is_archived = 0;
       $book->save();
       $request->session()->flash('message', 'The book has been added');
       return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = books::find($id);
        return view('items.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = books::find($id);

        $book->name = $request->name;
        $book->authorName = $request->author;
        $book->date_released = $request->date_released;
        $book->genre = $request->genre;
        $book->description = $request->description;


        if ($request->hasFile('image')) {
        	Storage::disk('public')->delete($book->display);
        	$path = $request->image->store('images','public');
        	$book->display = $path;
        	
        }
        $book->save();
        $request->session()->flash('message','The book has been updated');
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $book = books::find($id);
        $book->is_archived = 1;
        $book->save();
        $request->session()->flash('message', 'The item has been deleted');
        return redirect('/home');
    }

    public function deleteConfirm($id)
     {
        $book = books::find($id);
        return view('items.delete', ['book' => $book]);
    }

    public function getBest()
    {
    	$book = books::orderBy('views', 'DESC')->get();
    	$genres = books::distinct()->get('genre');
    	return view('home', ['books'=>$book], ['genres' => $genres]);

    }

    public function getNew()
    {
    	$book = books::orderBy('date_released', 'DESC')->get();
    	$genres = books::distinct()->get('genre');
    	return view('home', ['books' => $book], ['genres'=>$genres ] );
    }

    public function getGenre($genre)
    {
       	$book = DB::table('books')->where('genre', $genre)->get();
       	$genres = books::distinct()->get('genre');	
        return view('home', ['books' => $book], ['genres' => $genres]); 
    }

    public function searchManga(Request $request)
    {
    	$mangaName = $request->mangaName;
    	$books = DB::table('books')->where('name', $mangaName)->get();
       	$genres = books::distinct()->get('genre');	
        return view('home', ['books' => $books], ['genres' => $genres]);
    }

    public function createRent(Request $request, $id)
    {
    	$books = books::find($id);
    	return view('items.rentCreate', ['books'=>$books]);

    }

    public function storeRent(Request $request, $id)
    {
    	$borrowDetails = new borrow_requests;
    	$user = Auth::id();
    	$name = Auth::user();
    	$borrowDetails->user_id = $user;
        $borrowDetails->book_id = $id;
        $borrowDetails->date_borrowed = NOW();
        $borrowDetails->date_returned = NOW();
        $borrowDetails->receive_by = $name->name;
        $borrowDetails->release_by = '';
        $borrowDetails->status = 'borrowed';
        $borrowDetails->save();
        $request->session()->flash('message','The book has been successfuly rented');
        return redirect('/home');
    }
}
