<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'BooksController@index');
Route::get('/items/create', 'BooksController@create');
Route::post('/items/store', 'BooksController@store');
Route::get('/items/{id}/edit', 'BooksController@edit');
Route::put('/items/{id}', 'BooksController@update');
Route::get('/items/{id}/delete-confirm', 'BooksController@deleteConfirm');
Route::delete('/items/{id}', 'BooksController@destroy');
Route::get('/items/best', 'BooksController@getBest');
Route::get('/items/new', 'BooksController@getNew');
Route::get('/items/{genre}/', 'BooksController@getGenre');
Route::post('/items/manga-list/', 'BooksController@searchManga');
Route::get('/items/{id}/rentConfirm/', 'BooksController@createRent');
Route::put('/items/{id}', 'BooksController@storeRent');
