@extends('layouts.app')
@section('title', 'Menu')
@section('content')
@extends('layouts.header')
    <div class="container-fluid">
    @csrf    
        <h3>Menu</h3>
        <h3>Welcome {{ Auth::user()->name }}</h3>
        @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
            <a href="{{ url('items/create') }}" class="btn btn-primary">Add Item</a>    
        @endif
        
            <div class="row">
                @foreach ($books as $book)
                    @if ($book->is_archived != 1)
                        <div class="col-3 mt-3">
                            <div class= "card h-100">
                                <img src= '{{ asset("storage/$book->display") }}' class="card-img-top" width="100%" height="200px">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $book->name }}</h4>
                                    <p class="card-text" style="height:41%">{{ $book->description }}</p>
                                    <p class="card-text"></a>{{ $book->genre }}</p>
                                    <p class="card-text"></a>{{ $book->views }}</p>
                                    @if(!empty(Auth::user()))
                                        @if(Auth::user()->user_role == "admin")
                                            <div class="btn-group btn-block align-bottom">
                                                <a class="btn btn-outline-info" href='{{ url("items/$book->id/edit") }}'>Edit</a>

                                                <a class="btn btn-outline-danger" href='{{ url("items/$book->id/delete-confirm") }}'>Delete</a>
                                            </div>
                                        @elseif(Auth::user()->user_role == "customer")
                                            <div class="btn-group btn-block">
                                                <input type="number" class="form-control" name="quantity" value="1" min="1" required>
                                                    <a class="btn btn-success"  href='{{ url("items/$book->id/rentConfirm") }}'>Rent</a>
                                            </div>
                                            <div class="btn-group btn-block">
                                                <input type="number" class="form-control" name="quantity" value="1" min="1" required>
                                                    <a class="btn btn-success"  href='{{ url("items/$book->id/rentConfirm") }}'>Return</a>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    
@endsection

@if (!empty(session()->get('message')))
    <script>alert('{{ session()->get("message") }}')</script>
@endif

