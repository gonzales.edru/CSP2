<nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-center">
  <a class="navbar-brand mb-3" href="{{ url('/home') }}">Manga-rent</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div>
      <ul class="navbar-nav mr-auto">
        
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/register') }}">Register</a>
        </li>
        @guest
        <li class="nav-item dropdown">
          <li class="nav-item">
          <a class="nav-link" href="{{ url('/login') }}">Login</a>
        </li>
        @else
        <li class="nav-item">
          <a onclick="document.querySelector('#logout-form').submit()" href="#" class="nav-link">Logout</a>
        </li>
        @endguest
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Genre
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
        @foreach($genres as $genre)
          <a class="dropdown-item" href='{{ url("/items/$genre->genre") }}' >{{ $genre->genre }}</a>
        @endforeach
        </div>
      </li>
      <li class="nav-item dropdown">
          <li class="nav-item">
          <a class="nav-link" href="{{ url('/items/best') }}">Best</a>
        </li>
        <li class="nav-item dropdown">
          <li class="nav-item">
          <a class="nav-link" href="{{ url('items/new') }}">New</a>
        </li>
        <form class="d-flex m-auto" action="{{ url('/items/manga-list/') }}" method="post" enctype="multipart/form-data">
          @csrf
          @method("POST")
          <div class="form-group d-flex">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="mangaName" required>
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>  
          </div>
        </form> 
      </ul>
    </div>

</nav>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

