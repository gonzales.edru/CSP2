@extends('layouts.app')
@section('title', 'Add Manga')
@section('rent-manga-form')

	<form action='{{ url("items/$books->id") }}' method="post" enctype="multipart/form-data">
		@csrf
		@method("PUT")
		<div class="form-group">
			
			<label>

				Book Name
			
			</label>
			<input type="text" class="form-control" name="name" value="{{ $books->name }}" required disabled>
		</div>

		<div class="form-group">
			
			<label>
				Author
			</label>
			<input type="text" class="form-control" name="author" value="{{ $books->authorName }}" required disabled>
		</div>

		<div class="form-group">
			
			<label>
				date_released
			</label>
			<input type="date" class="form-control" name="date_released" value="{{ $books->date_released }}" required disabled>
		</div>

		<div class="form-group">
			
			<label>
				genre
			</label>
			<input type="text" class="form-control" name="genre" value="{{ $books->genre }}" required disabled>

		</div>

		<div class="form-group">
			
			<label>
				description
			</label>
			<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="resize:none;height=100%" disabled>{{ $books->description }}</textarea>
			

		</div>
		<button type="submit" class="btn btn-success btn-block">Rent</button>

	</form>

@endsection

@section('content')

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-6 mx-auto">
				<div class="card">
					
					<div class="card-header text-center">
						Manga Information
					</div>
					<div class="card-body">@yield('rent-manga-form')
					</div>

				</div>

			</div>

		</div>

	</div>

@endsection