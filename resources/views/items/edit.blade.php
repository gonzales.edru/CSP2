@if(Auth::user()->user_role != 'admin')
	<script>
		window.location = '/home'
	</script>
@endif

@extends('layouts.app')
@section('title', 'Edit Manga')
@section('edit-manga-form')

	<form action='{{ url("items/$book->id") }}' method="post" enctype="multipart/form-data">
		@csrf
		@method("PUT")
		<div class="form-group">
			
			<label>

				Book Name
			
			</label>
			<input type="text" class="form-control" name="name" value="{{ $book->name }}" required>
		</div>

		<div class="form-group">
			
			<label>
				Author
			</label>
			<input type="text" class="form-control" name="author" value="{{ $book->authorName }}" required>
		</div>

		<div class="form-group">
			
			<label>
				date_released
			</label>
			<input type="date" class="form-control" name="date_released" value="{{ $book->date_released }}" required>
		</div>

		<div class="form-group">
			
			<label>
				genre
			</label>
			<input type="text" class="form-control" name="genre" value="{{ $book->genre }}" required>

		</div>

		<div class="form-group">
			
			<label>
				description
			</label>
			<input type="text" class="form-control" name="description" value="{{ $book->description }}" required> 

		</div>

		<div class="form-group">
			
			<label>
				Image
			</label>
			<input type="file" class="form-control" name="image">

		</div>
		<button type="submit" class="btn btn-success btn-block">Update</button>

	</form>

@endsection

@section('content')

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-6 mx-auto">
				<h3 class="text-center">Edit Manga</h3>
				<div class="card">
					
					<div class="card-header text-center">
						Manga Information
					</div>
					<div class="card-body">
						@yield('edit-manga-form')
					</div>

				</div>

			</div>

		</div>

	</div>

@endsection