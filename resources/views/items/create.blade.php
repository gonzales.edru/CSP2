@if(Auth::user()->user_role != 'admin')
	<script>
		window.location = '/home'
	</script>
@endif

@extends('layouts.app')
@section('title', 'Add Manga')
@section('add-manga-form')

	<form action="{{ url('items/store') }}" method="post" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			
			<label>

				Book Name
			
			</label>
			<input type="text" class="form-control" name="name" required>
		</div>

		<div class="form-group">
			
			<label>
				Author
			</label>
			<input type="text" class="form-control" name="author" required>
		</div>

		<div class="form-group">
			
			<label>
				date_released
			</label>
			<input type="date" class="form-control" name="date_released" required>
		</div>

		<div class="form-group">
			
			<label>
				genre
			</label>
			<input type="text" class="form-control" name="genre" required>

		</div>

		<div class="form-group">
			
			<label>
				description
			</label>
			<input type="text" class="form-control" name="description" required>

		</div>

		<div class="form-group">
			
			<label>
				Image
			</label>
			<input type="file" class="form-control" name="image" required>

		</div>
		<button type="submit" class="btn btn-success btn-block">Add</button>

	</form>

@endsection

@section('content')

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-6 mx-auto">
				<div class="card">
					
					<div class="card-header text-center">
						Manga Information
					</div>
					<div class="card-body">@yield('add-manga-form')
					</div>

				</div>

			</div>

		</div>

	</div>

@endsection